/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duck.animal;

/**
 *
 * @author Administrator
 */
public class Crab extends AquaticAnimal {
    private String nickname;
    public Crab(String nickname) {
        super("Crab");
    }

    @Override
    public void swim() {
        System.out.println("Crab : "+nickname+" swim");
    }

    @Override
    public void eat() {
        System.out.println("Crab : "+nickname+" eat");
    }

    @Override
    public void walk() {
        System.out.println("Crab : "+nickname+" walk");
    }

    @Override
    public void speak() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void sleep() {
        System.out.println("Crab : "+nickname+" sleep");
    }
}