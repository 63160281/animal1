/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duck.animal;

/**
 *
 * @author Administrator
 */
public class Snake extends Reptile {
    private String nickname;
    public Snake(String nickname) {
        super("Crocodile", 0);
    }

    @Override
    public void crawl() {
        System.out.println("Snake : "+nickname+" crawl");
    }

    @Override
    public void eat() {
        System.out.println("Snake : "+nickname+" eat");
    }
    @Override
    public void speak() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void sleep() {
        System.out.println("Snake : "+nickname+" sleep");
    }

    @Override
    public void walk() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

