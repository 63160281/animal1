/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duck.animal;

/**
 *
 * @author Administrator
 */
public class Fish extends AquaticAnimal {
    private String nickname;
    public Fish(String nickname) {
        super("Fish");
    }

    @Override
    public void swim() {
        System.out.println("Fish : "+nickname+" swim");
    }

    @Override
    public void eat() {
        System.out.println("Fish : "+nickname+" eat");
    }

    @Override
    public void walk() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void speak() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void sleep() {
        System.out.println("Fish : "+nickname+" sleep");
    }
    
}
