/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duck.animal;

/**
 *
 * @author Administrator
 */
public class Cat extends LandAnimal {
    private String nickname;
    public Cat(String nickname) {
        super("Cat", 4);
        this.nickname = nickname; 
    }

    @Override
    public void run() {
        System.out.println("cat : "+nickname+" run");
    }

    @Override
    public void eat() {
       System.out.println("cat : "+nickname+" eat") ;    
    }       
    @Override
    public void walk() {
        System.out.println("cat : "+nickname+" walk");
    }
    @Override
    public void sleep() {
        System.out.println("cat : "+nickname+" Sleep");
    }            

    @Override
    public void speak() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
}
