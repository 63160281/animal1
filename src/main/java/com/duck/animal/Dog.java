/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duck.animal;

/**
 *
 * @author Administrator
 */
public class Dog extends LandAnimal {
    private String nickname;
    public Dog(String nickname) {
        super("Dog", 4);
        this.nickname = nickname; 
    }

    @Override
    public void run() {
        System.out.println("Dog : "+nickname+" run");
    }

    @Override
    public void eat() {
       System.out.println("Dog : "+nickname+" eat") ;    
    }       
    @Override
    public void walk() {
        System.out.println("Dog : "+nickname+" walk");
    }
    @Override
    public void sleep() {
        System.out.println("Dog : "+nickname+" Sleep");
    }            

    @Override
    public void speak() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
}
